// rajoute un conteneur pour l'autocompletion
const autocomplete = document.createElement('div');
autocomplete.classList.add("autocomplete-results");
document.querySelector('#query').after(autocomplete);

// crée une référence vers la liste des utilisateurs
const ul = document.querySelector('#userlist');

// listener sur la saisie de texte
document.querySelector('#query').addEventListener('keyup', () => {
    // on vide les résultats précédents
    autocomplete.innerHTML = '';

    // récupère le texte dans l'input
    const query = encodeURI(document.querySelector('#query').value);

    // on effectue la requête
    fetch(`/api/users/autocomplete?q=${query}`).then( response => {
        // désérialise la réponse json
        return response.json();
    }).then( users => {
        // ajoute les éléments à la liste
        users.forEach( user => {
            const element = document.createElement('div');
            // ajoute le texte
            element.appendChild(document.createTextNode(`${user.firstname} ${user.name} (${user.username})`));

            // lors du clic sur l'élément, on souhaite l'ajouter à la liste des utilisateurs
            element.addEventListener('click', () => {
                addUser(user);
            });

            // ajoute le li au ul
            autocomplete.appendChild(element);
        });
    });
});

// utilisé pour ajouter un utilisateur à la liste
function addUser(user) {
    // création du li à ajouter
    const element = document.createElement('li');
    element.appendChild(document.createTextNode(`${user.firstname} ${user.name}`));

    // pour alimenter le formulaire, on rajoute en hidden l'id de l'utilisateur
    const hidden = document.createElement('input');
    hidden.setAttribute("type", "hidden");
    hidden.setAttribute("name", "userIds");
    hidden.setAttribute("value", user.id);
    element.appendChild(hidden);

    // bouton de suppression
    const remove = document.createElement('button');
    remove.setAttribute("type", "button");
    remove.setAttribute("class", "close");
    remove.setAttribute("aria-label", "Close");
    remove.innerHTML = '<span aria-hidden="true">&times;</span>';
    remove.addEventListener('click', () => { removeUser(remove); });
    element.appendChild(remove);

    // enfin, ajoute l'élément à la liste des utilisateurs
    ul.appendChild(element);
}

// utilisé pour supprimer un utilisateur à la liste
function removeUser(e) {
    // e est le button qui a été cliqué
    // récupère le ul
    const ul = e.closest('ul');

    // li à supprimer
    const li = e.closest('li');

    // supprime l'utilisateur
    ul.removeChild(li);
}
