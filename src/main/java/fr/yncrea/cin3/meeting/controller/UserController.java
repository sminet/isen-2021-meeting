package fr.yncrea.cin3.meeting.controller;

import fr.yncrea.cin3.meeting.domain.User;
import fr.yncrea.cin3.meeting.form.UserForm;
import fr.yncrea.cin3.meeting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("/user")
@Secured("ROLE_ADMIN")
public class UserController {
    @Autowired
    private UserRepository users;

    @GetMapping({"/", "/list"})
    public String list(Model model) {
        model.addAttribute("users", users.findAll());
        return "user/list";
    }

    @GetMapping({"/create", "/update/{id}"})
    public String create(@PathVariable(required = false) UUID id, Model model) {
        var form = new UserForm();
        model.addAttribute("form", form);

        if (id != null) {
            User u = users.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
            form.setId(u.getId());
            form.setUsername(u.getUsername());
            form.setFirstname(u.getFirstname());
            form.setName(u.getName());
        }

        return "user/create";
    }

    @PostMapping("/create")
    public String createPost(@Valid @ModelAttribute("form") UserForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("form", form);
            return "user/create";
        }

        User u = new User();
        if (form.getId() != null) {
            u = users.findById(form.getId()).orElseThrow(() -> new RuntimeException("User not found"));
        }

        u.setUsername(form.getUsername());
        u.setFirstname(form.getFirstname());
        u.setName(form.getName());
        users.save(u);
        return "redirect:/user/list";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable UUID id) {
        users.deleteById(id);
        return "redirect:/user/list";
    }
}
