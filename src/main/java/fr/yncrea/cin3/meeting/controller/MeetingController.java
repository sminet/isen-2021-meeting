package fr.yncrea.cin3.meeting.controller;

import fr.yncrea.cin3.meeting.domain.Meeting;
import fr.yncrea.cin3.meeting.form.MeetingInviteForm;
import fr.yncrea.cin3.meeting.domain.User;
import fr.yncrea.cin3.meeting.form.MeetingForm;
import fr.yncrea.cin3.meeting.repository.MeetingRepository;
import fr.yncrea.cin3.meeting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.*;

@Controller
@RequestMapping("/meeting")
public class MeetingController {
    @Autowired
    private MeetingRepository meetings;

    @Autowired
    private UserRepository users;

    @GetMapping({"/", "/list"})
    public String list(Model model, @PageableDefault(page = 0, size = 5) Pageable pageable) {
        model.addAttribute("meetings", meetings.findAllSummary(pageable));

        return "meeting/list";
    }

    @GetMapping({"/create", "/update/{id}"})
    public String create(@PathVariable(required = false) UUID id, Model model) {
        var form = new MeetingForm();
        model.addAttribute("form", form);

        if (id != null) {
            Meeting m = meetings.findById(id).orElseThrow(() -> new RuntimeException("Meeting not found"));
            form.setId(m.getId());
            form.setName(m.getName());
            form.setDescription(m.getDescription());
            form.setDuration(m.getDuration());
        }

        return "meeting/create";
    }

    @PostMapping("/create")
    public String createPost(@Valid @ModelAttribute("form") MeetingForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("form", form);
            return "meeting/create";
        }

        Meeting m = new Meeting();
        if (form.getId() != null) {
            m = meetings.findById(form.getId()).orElseThrow(() -> new RuntimeException("Meeting not found"));
        }

        m.setName(form.getName());
        m.setDescription(form.getDescription());
        m.setDuration(form.getDuration());
        m.setDate(Instant.now()); // TODO read the date from the form

        meetings.save(m);
        return "redirect:/meeting/list";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable UUID id) {
        meetings.deleteById(id);
        return "redirect:/meeting/list";
    }

    @GetMapping("/invite/{id}")
    public String invite(@PathVariable UUID id, Model model) {
        var form = new MeetingInviteForm();
        var meeting = meetings.findById(id).orElseThrow(() -> new RuntimeException("Meeting not found"));

        // set meeting id
        form.setMeetingId(meeting.getId());

        // fill in already invited users
        Set<UUID> userIds = new LinkedHashSet<>();
        for (User u: meeting.getUsers()) {
            userIds.add(u.getId());
        }
        form.setUserIds(userIds);

        // add everything we need to the model
        model.addAttribute("form", form);
        model.addAttribute("meeting", meeting);
        model.addAttribute("users", users.findAllById(userIds));

        return "meeting/invite";
    }

    @PostMapping("/invite")
    public String invitePost(@ModelAttribute MeetingInviteForm form) {
        Meeting meeting = meetings.findById(form.getMeetingId()).orElseThrow(() -> new RuntimeException("Meeting not found"));

        List<User> participants = new ArrayList<>();
        for (UUID userId: form.getUserIds()) {
            // using getById, which do not fetch the user from database
            //  since it is not required to simply add a relation
            participants.add(users.getById(userId));
        }

        meeting.setUsers(participants);

        meetings.save(meeting);

        return "redirect:/meeting/list";
    }
}
