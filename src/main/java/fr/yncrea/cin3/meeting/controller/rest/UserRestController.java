package fr.yncrea.cin3.meeting.controller.rest;

import com.fasterxml.jackson.annotation.JsonView;
import fr.yncrea.cin3.meeting.domain.User;
import fr.yncrea.cin3.meeting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    @Autowired
    private UserRepository users;

    @GetMapping("/autocomplete")
    @JsonView(User.UserViews.BasicData.class)
    public Set<User> autocomplete(@RequestParam("q") String query) {
        return users.findAutocomplete(query);
    }
}
