package fr.yncrea.cin3.meeting.form;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.UUID;

@Getter
@Setter
public class MeetingInviteForm {
    private UUID meetingId;
    private Set<UUID> userIds;
}
