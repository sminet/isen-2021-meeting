package fr.yncrea.cin3.meeting.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class UserForm {
    private UUID id;

    private String username;

    private String firstname;

    @Size(min = 2, max = 20)
    private String name;
}
