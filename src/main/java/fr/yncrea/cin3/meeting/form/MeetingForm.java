package fr.yncrea.cin3.meeting.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class MeetingForm {
    private UUID id;

    @NotBlank
    @Size(min = 10)
    private String name;

    private String description;

    @Min(0)
    private Long duration;
}
