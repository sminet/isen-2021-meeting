package fr.yncrea.cin3.meeting.repository;

import fr.yncrea.cin3.meeting.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    User findByUsername(String username);

    Set<User> findFirst10ByFirstnameContainingIgnoreCaseOrNameContainingIgnoreCase(String firstname, String name);

    default Set<User> findAutocomplete(String query) {
        return this.findFirst10ByFirstnameContainingIgnoreCaseOrNameContainingIgnoreCase(query, query);
    }
}
