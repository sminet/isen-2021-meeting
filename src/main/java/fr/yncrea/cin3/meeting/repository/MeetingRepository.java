package fr.yncrea.cin3.meeting.repository;

import fr.yncrea.cin3.meeting.domain.Meeting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface MeetingRepository extends JpaRepository<Meeting, UUID> {
    @Query("select m.id from Meeting m")
    public Page<UUID> findIds(Pageable page);

    @Query("select distinct m from Meeting m left join fetch m.users u where m.id in (:ids)")
    public List<Meeting> findAllByIdFetchingUsers(Set<UUID> ids);

    default Page<Meeting> findAllSummary(Pageable pageable) {
        var page = findIds(pageable);
        var meetings = findAllByIdFetchingUsers(page.toSet());

        return new PageImpl<>(meetings, pageable, page.getTotalElements());
    }
}
