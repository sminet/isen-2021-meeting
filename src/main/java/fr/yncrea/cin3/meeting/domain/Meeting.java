package fr.yncrea.cin3.meeting.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
public class Meeting {
    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    private String description;

    private Instant date;

    private Long duration;

    @ManyToMany
    private List<User> users;
}
