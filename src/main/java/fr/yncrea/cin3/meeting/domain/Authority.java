package fr.yncrea.cin3.meeting.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
@Entity
public class Authority implements GrantedAuthority {
    @Id
    @GeneratedValue
    private UUID id;

    private String authority;
}
