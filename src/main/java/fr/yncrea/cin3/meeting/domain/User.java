package fr.yncrea.cin3.meeting.domain;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "\"user\"")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class User implements UserDetails {
    public interface UserViews {
        interface BasicData {}
        interface ExtendedData extends BasicData {}
    }

    @Id
    @GeneratedValue
    @JsonView(UserViews.BasicData.class)
    private UUID id;

    @Column(nullable = false, unique = true)
    @JsonView(UserViews.BasicData.class)
    private String username;

    @Column(length = 60)
    private String password;

    @JsonView(UserViews.BasicData.class)
    private String firstname;

    @JsonView(UserViews.BasicData.class)
    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Authority> authorities;

    @Override
    @JsonView(UserViews.ExtendedData.class)
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonView(UserViews.ExtendedData.class)
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonView(UserViews.ExtendedData.class)
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonView(UserViews.ExtendedData.class)
    public boolean isEnabled() {
        return true;
    }
}
